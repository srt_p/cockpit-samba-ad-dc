import cockpit from 'cockpit';
import React from 'react';
import {
    Nav,
    NavItem,
    Divider,
    NavList,
    Page,
    PageSection,
    Stack,
} from '@patternfly/react-core';
import { Domain } from './samba/domain.jsx';
import { User } from './samba/user.jsx';

const _ = cockpit.gettext;

function PageContent(props) {
    if (!props.content) {
        return null;
    }

    if (props.content === 'Domain') {
        return (
            <Stack hasGutter>
                <Domain />
            </Stack>
        );
    }
    if (props.content === 'User') {
        return (
            <Stack hasGutter>
                <User />
            </Stack>
        );
    }
    return null;
}

export class Application extends React.Component {
    constructor() {
        super();
        this.state = {
            hostname: _("Unknown"),
            activeItem: 0,
        };

        this.navitems = [
            { name: 'Domain' },
            { name: 'User' },
            { name: 'Groups' },
        ];
    }

    render() {
        return (
            <>
                <Nav variant="horizontal">
                    <NavList>
                        {this.navitems.map((_item, index) => {
                            return (
                                <React.Fragment key={index}>
                                    {index !== 0 && (
                                        <Divider
                                            orientation={{ default: 'vertical' }}
                                            className="samba_ad_dc_navdivider"
                                        />
                                    )}
                                    <NavItem
                                        preventDefault
                                        onClick={() => this.setState({ activeItem: index })}
                                        itemId={index}
                                        isActive={this.state.activeItem === index}
                                        id={`horizontal-subnav-${index}`}
                                        to={`#horizontal-subnav-${index}`}
                                        className="samba_ad_dc_navitem"
                                    >
                                        {_item.name}
                                    </NavItem>
                                </React.Fragment>
                            );
                        })}
                    </NavList>
                </Nav>
                <Page>
                    <PageSection>
                        <PageContent content={this.navitems[this.state.activeItem].name} />
                    </PageSection>
                    <br />
                </Page>
            </>
        );
    }
}
