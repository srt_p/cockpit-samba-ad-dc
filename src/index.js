import "cockpit-dark-theme";
import "patternfly/patternfly-5-cockpit.scss";

import React from 'react';
import { createRoot } from 'react-dom/client';
import { Application } from './app.jsx';
import './app.scss';

document.addEventListener("DOMContentLoaded", () => {
    createRoot(document.getElementById("app")).render(<Application />);
});
