import cockpit from 'cockpit';
import React, { useState } from 'react';
import {
    Stack,
    Card,
    CardBody,
    CardTitle,
    CardExpandableContent,
    CardHeader,
    Form,
    FormGroup,
    FormHelperText,
    HelperText,
    HelperTextItem,
    TextInput,
    Alert,
    ActionGroup,
    Button,
    CodeBlock,
    CodeBlockCode,
} from '@patternfly/react-core';

function Info() {
    return (
        <Card>
            <CardHeader>
                <CardTitle>
                    Domain Info
                </CardTitle>
            </CardHeader>
            <CardExpandableContent>
                <CardBody>
                    Info
                </CardBody>
            </CardExpandableContent>
        </Card>
    );
}

function Provision() {
    const [isExpanded, setIsExpanded] = useState(false);
    const [realm, setRealm] = useState('SAMDOM.EXAMPLE.COM');
    const [domain, setDomain] = useState('samdom');
    const [provisionOutput, setProvisionOutput] = useState('');
    const [systemctlOutput, setSystemctlOutput] = useState('');
    const [isProvisionRunning, setIsProvisionRunning] = useState(false);

    const onProvisionToggle = () => { setIsExpanded(!isExpanded) };
    const handleRealmChange = (_event, realm) => { setRealm(realm.toUpperCase()) };
    const handleDomainChange = (_event, domain) => { setDomain(domain) };
    const provision = () => {
        setProvisionOutput('');
        setSystemctlOutput('');
        setIsProvisionRunning(true);
        const processSambaTool = cockpit.spawn(
            [
                'samba-tool', 'domain', 'provision',
                '--use-rfc2307',
                '--realm', realm,
                '--domain', domain,
                '--server-role', 'dc',
                '--dns-backend', 'SAMBA_INTERNAL',
                '--adminpass', '1A2b3c4d'
            ],
            {
                err: 'message',
                superuser: 'require'
            }
        );
        processSambaTool.stream(data => {
            setProvisionOutput(provisionOutput => provisionOutput + data);
            return data.length;
        });
        processSambaTool.then(data => {
            setProvisionOutput(provisionOutput => provisionOutput + data);
            const processSystemctl = cockpit.spawn(
                ['systemctl', 'restart', 'samba'],
                {
                    err: 'message',
                    superuser: 'require'
                }
            );
            processSystemctl.stream(data => {
                setSystemctlOutput(systemctlOutput => systemctlOutput + data);
                return data.length;
            });
            processSystemctl.then(data => {
                setSystemctlOutput(systemctlOutput => systemctlOutput + data);
            });
            processSystemctl.catch(exception => {
                if (exception.problem) {
                    setSystemctlOutput(systemctlOutput => systemctlOutput + 'problem starting/communicating with the process: ' + exception.problem);
                } else {
                    setSystemctlOutput(systemctlOutput => systemctlOutput + exception.message);
                }
            });
            setIsProvisionRunning(false);
        });
        processSambaTool.catch(exception => {
            if (exception.problem) {
                setProvisionOutput(provisionOutput => provisionOutput + 'problem starting/communicating with the process: ' + exception.problem);
            } else {
                setProvisionOutput(provisionOutput => provisionOutput + exception.message);
            }
            setIsProvisionRunning(false);
        });
    };

    return (
        <Card isExpanded={isExpanded}>
            <CardHeader
                onExpand={onProvisionToggle} toggleButtonProps={{ 'aria-label': 'Details' }}
            >
                <CardTitle
                    onClick={onProvisionToggle}
                    className="samba_ad_dc_expandable_card_header"
                >
                    Domain Provision
                </CardTitle>
            </CardHeader>
            <CardExpandableContent>
                <CardBody>
                    <Alert
                        variant="warning"
                        title="Do not provision a domain if you already have a domain"
                    />
                    <br /><br />
                    <Form isHorizontal>
                        <FormGroup label="Realm" isRequired>
                            <TextInput
                                value={realm}
                                isRequired
                                type="text"
                                onChange={handleRealmChange}
                            />
                            <FormHelperText>
                                <HelperText>
                                    <HelperTextItem>Kerberos realm. The uppercase version of the AD DNS domain. For example: SAMDOM.EXAMPLE.COM</HelperTextItem>
                                </HelperText>
                            </FormHelperText>
                        </FormGroup>
                        <FormGroup label="Domain" isRequired>
                            <TextInput
                                value={domain}
                                isRequired
                                type="text"
                                onChange={handleDomainChange}
                            />
                            <FormHelperText>
                                <HelperText>
                                    <HelperTextItem>NetBIOS domain name (Workgroup). This can be anything, but it must be one word, not longer than 15 characters and not containing a dot. It is recommended to use the first part of the AD DNS domain. For example: samdom. Do not use the computers short hostname.</HelperTextItem>
                                </HelperText>
                            </FormHelperText>
                        </FormGroup>
                        <ActionGroup>
                            <Button
                                variant="primary"
                                onClick={provision}
                                isLoading={isProvisionRunning}
                            >
                                Provision
                            </Button>
                        </ActionGroup>
                        {provisionOutput !== "" && (
                            <CodeBlock>
                                <CodeBlockCode>
                                    {provisionOutput}
                                </CodeBlockCode>
                            </CodeBlock>
                        )}
                        {systemctlOutput !== "" && (
                            <CodeBlock>
                                <CodeBlockCode>
                                    {systemctlOutput}
                                </CodeBlockCode>
                            </CodeBlock>
                        )}
                    </Form>
                </CardBody>
            </CardExpandableContent>
        </Card>
    );
}

export function Domain() {
    return (
        <Stack hasGutter>
            <Info />
            <Provision />
        </Stack>
    );
}
